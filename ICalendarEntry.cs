using System;
using System.Collections.Generic;

namespace BLL
{
    // Interface is used with the calendar to share data between systems and different calendar parts/views
    // The calendar entries are displayed with the imported entries from an outside system
    public interface ICalendarEntry
    {
        //int CalendarSyncEntryID { get; set; }
        int CalendarEntryID { get; set; }
        int? CalendarEntryCategoryID { get; set; }
        int? OfficeID { get; set; }
        DateTime? StartTime { get; set; }
        DateTime? EndTime { get; set; }
        string Notes { get; set; }
        string Title { get; set; }
        string RecurrenceRule { get; set; }
        int? RecurrenceParentID { get; set; }
        DateTime? DateCreated { get; set; }
        bool? IsAllDay { get; set; }
        int? TeamCreated { get; set; }
        int TeamID { get; set; }
        string Attendees { get; set; }
        string Color { get; set; }
        bool? IsOutsideAppointment { get; set; }
        int? CaseTaskID { get; set; }
        List<int> AttendeeIDs { get; set; }
    }


    // some gentle nudging so all our calendar flavors comform to the interface
    public partial class CalendarEntry : ICalendarEntry
    {
        public int TeamID { get { return -1; } set { return; } }
        public string Attendees { get { return ""; } set { return; } }
        public bool? IsOutsideAppointment { get; set; }
        public string Color { get; set; }
        public List<int> AttendeeIDs { get; set; }
    }
    public partial class vw_CalendarEntry : ICalendarEntry
    {
        public bool? IsOutsideAppointment { get; set; }
        public List<int> AttendeeIDs { get; set; }
    }
    public partial class vw_CalendarEntries_All : ICalendarEntry
    {
        public int TeamID { get { return -1; } set { return; } }
        public bool? IsOutsideAppointment { get; set; }
        public List<int> AttendeeIDs { get; set; }
    }
}