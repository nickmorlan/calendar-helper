using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL.Calendar
{
    public class OutsideCalendarAppointment
    {
        private DALDataContext _db;
        private string _status;
        private string _message;

        public OutsideCalendarAppointment()
        {
            _db = new DALDataContext(BLL.Properties.Settings.Default.GoldenReserve_CRM_BetaConnectionString);
        }

        public OutsideCalendarAppointment(string connection)
        {
            _db = new DALDataContext(connection);
        }

        public List<ICalendarEntry> GetOutsideRecords(DateTime start, DateTime end)
        {
            return PrepResults(_db.vw_CalendarEntries.Where(c => c.StartTime >= start && c.StartTime <= end).ToList<ICalendarEntry>());
        }

        public List<ICalendarEntry> GetOutsideRecords(DateTime start, DateTime end, List<int> attorneys)
        {
            return PrepResults(_db.vw_CalendarEntries.Where(c => c.StartTime >= start && c.StartTime <= end && attorneys.Contains(c.TeamID)).ToList<ICalendarEntry>());
        }

        public List<ICalendarEntry> GetOutsideRecordsWithRecurring(DateTime start, DateTime end)
        {
            List<ICalendarEntry> results = GetOutsideRecords(start, end);
            List<int> attorneys = _db.Teams.Where(x => !x.IsPP && x.IsActive.GetValueOrDefault()).Select(x => x.TeamID).ToList();
            results.AddRange(ProcessRecurring(start, end, attorneys));
            return PrepResults(results);
        }

        public List<ICalendarEntry> GetOutsideRecordsWithRecurring(DateTime start, DateTime end, List<int> attorneys)
        {
            List<ICalendarEntry> results = GetOutsideRecords(start, end, attorneys);
            results.AddRange(ProcessRecurring(start, end, attorneys));
            return PrepResults(results);
        }

        public List<CalendarEntry> GetOutsideRecurringCalendarEntries(List<int> Attendees)
        {
            return _db.CalendarEntryTeams.Where(x => Attendees.Contains(x.TeamID.Value) && x.CalendarEntry.RecurrenceRule != null).Select(x => x.CalendarEntry).ToList();
        }

        /// <summary>
        /// helper method to setup the recurring calendar events as a single entry with the associated team
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="attorneys"></param>
        /// <returns></returns>
        private List<ICalendarEntry> ProcessRecurring(DateTime start, DateTime end, List<int> attorneys)
        {
            List<CalendarEntry> recurring = _db.CalendarEntryTeams.Where(x => attorneys.Contains(x.TeamID.Value) && x.CalendarEntry.RecurrenceRule != null).Select(x => x.CalendarEntry).ToList();
            List<ICalendarEntry> results = new List<ICalendarEntry>();

            foreach (CalendarEntry rec in recurring)
            {
                // just in case a blank string value gets through
                if (String.IsNullOrWhiteSpace(rec.RecurrenceRule))
                    continue;

                Telerik.Web.UI.RecurrenceRule rule = Telerik.Web.UI.RecurrenceRule.TryParse(rec.RecurrenceRule);
                rule.SetEffectiveRange(start, end);
                //List<DateTime> dlt = (from DateTime rdt in rule.Occurrences where rdt >= startdate && rdt < enddate select rdt).ToList();
                if (rule.Range.RecursUntil < DateTime.Now)
                    continue;
                foreach (DateTime dl in rule.Occurrences)
                {
                    List<string> attendees = new List<string>();
                    List<int> attendeeIDs = new List<int>();
                    foreach (CalendarEntryTeam recclt in rec.CalendarEntryTeams)
                    {
                        attendees.Add((recclt.Team.Initials != "") ? recclt.Team.Initials : rec.Team.FirstName.Substring(0, 1) + recclt.Team.LastName.Substring(0, 1));
                        if(recclt.TeamID.HasValue)
                         attendeeIDs.Add(recclt.TeamID.Value);
                    }
                    if (results.Any(x => x.CalendarEntryID == rec.CalendarEntryID))
                            continue;


                    vw_CalendarEntry nce = new vw_CalendarEntry();
                    nce.Title = rec.Title;
                    nce.RecurrenceRule = "";
                    nce.CalendarEntryID = rec.CalendarEntryID;
                    nce.DateCreated = rec.DateCreated;
                    nce.Name = (rec.Office != null) ? rec.Office.Name : "";
                    nce.TeamID = attorneys.First();
                    nce.TeamCreated = attorneys.First();
                    nce.Attendees = string.Join(",", attendees);
                    nce.StartTime = dl;
                    nce.EndTime = dl.Add(rec.EndTime.Value.Subtract(rec.StartTime.Value));
                    nce.OfficeID = rec.OfficeID;

                    results.Add(nce);

                }
            }

            return results;
        }

        /// <summary>
        /// sets up the ICalendarEntry to identify it as an outside calendar record and massage data as needed
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>
        private List<ICalendarEntry> PrepResults(List<ICalendarEntry> results)
        {
            // if the teamid of the calendar entry is in the list
            // then we want to show some details about it
            foreach (ICalendarEntry c in results)
            {
                c.IsOutsideAppointment = true;
                if(c.CalendarEntryCategoryID == 15)
                    c.Title += " (EVENT)";
                c.CalendarEntryCategoryID = 16;
                c.Color = "Black";
                if (c.AttendeeIDs == null)
                    c.AttendeeIDs = _db.CalendarEntryTeams.Where(x => x.CalendarEntryID == c.CalendarEntryID && x.TeamID.HasValue).Select(x => x.TeamID.Value).ToList();
            }
            return results;
        }
    }
}
